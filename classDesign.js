class planATrip{
    
    get country() {
        return this._country; }
    get startLocation() {
        return this._startLocation; }
    get endLocation() {
        return this._endLocation; }  
    get startDate() {
        return this._startDate; }
    get endDate() {
        return this._endDate; }

    set country(newcountry) {
        this._country = newcountry; }
    set startLocation(newstartLocation) {
        this.startLocation = newstartLocation; }
    set endLocation(newendLocation) {
        this.endLocation = newendLocation; }
    set startDate(newstartDate) {
        this.startDate = newstartDate; }
    set endDate(newendDate) {
        this.endDate = newendDate; }

    fromData(data){
        this._country = data._country;
        this._startLocation = data._startLocation;
        this._endLocation = data._endLocation;
        this._startDate = data._startDate;
        this._endDate = data._endDate;
    }
}

class tripDetails{
    
    get origin() {
        return this._origin; }
    get destination() {
        return this._destination; }
    get date() {
        return this._date; }  
    get boardingTime() {
        return this._boardingTime; }
    get departureTime() {
        return this._departureTime; }
    get arrivalTime() {
        return this._arrivalTime; }
    get flightDuration() {
        return this._flightDuration; }
    get flightNumber() {
        return this._flightNumber; }
    get class() {
        return this._class; }
    get gate() {
        return this._gate; }
    get seatNumber() {
        return this._seatNumber; }
    get id() {
        return this._id; }

    set origin(neworigin) {
        this._origin = neworigin; }
    set destination(newdestination) {
        this.destination = newdestination; }
    set date(newdate) {
        this.date = newdate; }
    set boardingTime(newboardingTime) {
        this.boardingTime = newboardingTime; }
    set departureTime(newdepartureTime) {
        this.departureTime = newdepartureTime; }
    set arrivalTime(newarrivalTime) {
        this.arrivalTime = newarrivalTime; }
    set flightDuration(newflightDuration) {
        this.flightDuration = newflightDuration; }
    set flightNumber(newflightNumber) {
        this.flightNumber = newflightNumber; }
    set class(newclass) {
        this.class = newclass; }
    set gate(newgate) {
        this.gate = newgate; }
    set seatNumber(newseatNumber) {
        this._seatNumber = newseatNumber; }
    set id(newid) {
        this._id = newid; }
    
    fromData(data){
        this._origin = data._origin;
        this._destination = data._destination;
        this._date = data._date;
        this._boardingTime = data._boardingTime;
        this._departureTime = data._departureTime;
        this._arrivalTime = data._arrivalTime;
        this._flightDuration = data._flightDuration;
        this._flightNumber = data._flightNumber;
        this._class = data._class;
        this._gate = data._gate;
        this._seatNumber = data._seatNumber;
        this._id = data._id;
    }
}

class tripDetailsList{
    
    addtripDetails(id){
        this._tripDetails = id
    }
    removetripDetails(id){
        this._tripDetails(id).remove(newtripDetails)
        }
    gettripDetails(id){
        return this._tripDetails(id)
    }
}
